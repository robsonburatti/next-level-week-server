# How to clone de project and start the container docker

## Clone the project

```console
git clone git@gitlab.com:buratti-experiments/next-level-week/next-level-week-server.git
```

## Install the packages of dependencies of the project

```console
npm i
```

## Create image docker locale

```console
docker build -t registry.gitlab.com/buratti-experiments/next-level-week/next-level-week-server .
```

## Run container docker

```console
docker run -it -p 9000:3333 -v $(pwd):/src/server registry.gitlab.com/buratti-experiments/next-level-week/next-level-week-server
```

---

# Create project with structure node

```console
npm init -y
```

# Install the packages of dependencies of the your project

```console
npm i
```

# Create the file `Dockerfile` with the configurations

```yaml
FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y sqlite3 libsqlite3-dev
RUN npx knex migrate:latest
EXPOSE 3333
CMD [ "npm", "start" ]
```

# Create de image docker

```console
docker build -t <project-name-here> .
```

# Run your container docker

```console
docker run -it -p 9000:3333 -v $(pwd):/app <project-name-here>
```

# Project configuration scripts

## Settings of the execution environments

### Create the file with the settings of the project execution environments

```console
npx knex init
```

### How to run the database creation script

```console
npx knex migrate:make create_incidents
```

```console
npx knex migrate:make create_ongs
```

### How to know the creation status of the base creation scripts

```console
npx knex migrate:status
```

### How to run the migration scripts

```console
npx knex migrate:latest
```

## How to run the test

### How to run the test database creation script

#### Adjust in the configuration file of execution environments created with `knex` then it is necessary to execute the command to create the test environment

```console
npx knex migrate:latest
```
